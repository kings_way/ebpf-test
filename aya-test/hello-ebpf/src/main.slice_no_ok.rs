#![no_std]
#![no_main]

use aya_bpf::{
    helpers::*,
    maps::PerCpuArray,
    macros::{map, tracepoint},
    programs::TracePointContext,
};

#[map]
static mut BUFFER: PerCpuArray<[u8; 4096]> = PerCpuArray::with_max_entries(1, 0);

#[tracepoint(name="tracepoint_execve")]
pub fn execve(ctx: TracePointContext) -> u32 {
    let offset_arg_name = 16;

    if let Some(buffer_ptr) = unsafe { BUFFER.get_ptr_mut(0) }{
        let buf = unsafe { &mut *buffer_ptr };

        /* read filename the first time */
        let addr = unsafe { match ctx.read_at::<u64>(offset_arg_name){
                Ok(v) => v,
                _ => return 1
            }
        };
        let len_name = unsafe { match bpf_probe_read_str(addr as *const u8, buf){
                Ok(v) => v,
                _ => return 1
            }
        };

//        let name = unsafe { core::str::from_utf8_unchecked(&buf[..len_name]) };
//        info!(&ctx, "buf1, len: {}, str: {}", len_name, name);


        /* read filename the second time */
        //let len_name = 100;
        if len_name > 4096 {
            return 1
        }

        let len_name = unsafe { match bpf_probe_read_str(addr as *const u8, &mut buf[len_name..]){
                Ok(v) => v,
                _ => return 1
            }
        };

//        let name = unsafe { core::str::from_utf8_unchecked(&buf[..len_name]) };
//        info!(&ctx, "buf1, len: {}, str: {}", len_name, name);
    }
    0
}


#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    unsafe { core::hint::unreachable_unchecked() }
}
