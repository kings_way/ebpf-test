#![no_std]
#![no_main]

use aya_bpf::{
    helpers::*,
    maps::PerCpuArray,
    macros::{map, tracepoint},
    programs::TracePointContext,
};
use aya_log_ebpf::info;

#[map]
static mut BUFFER: PerCpuArray<[u8; 4096]> = PerCpuArray::with_max_entries(1, 0);

#[tracepoint(name="tracepoint_execve")]
pub fn execve(ctx: TracePointContext) -> u32 {
    let offset_arg_name = 16;
    let offset_arg_argv = 24;

    if let Some(buffer_ptr) = unsafe { BUFFER.get_ptr_mut(0) }{
        let _buf = unsafe { &mut *buffer_ptr };
        let buf = &mut _buf[..2048];

        // filename
        let ptr_name = unsafe { match ctx.read_at::<*const u8>(offset_arg_name){
                Ok(v) => v,
                _ => return 1
            }
        };
        // argv[0]
        let ptr_argv0 = unsafe { match ctx.read_at::<*const *const u8>(offset_arg_argv){
                Ok(v) => match bpf_probe_read::<*const u8>(v){
                    Ok(v) => v,
                    _ => return 1
                },
                _ => return 1
            }
        };


        /* read filename into buffer */
        let offset1 = unsafe { match bpf_probe_read_user_str(ptr_name, buf){
                Ok(v) => v,
                _ => return 1
            }
        };


        /* read argv0 the second time */
        let offset2 = unsafe { match bpf_probe_read_str(ptr_argv0, &mut buf[offset1..]){
                Ok(v) => v,
                _ => return 1
            }
        };

        let name = unsafe { core::str::from_utf8_unchecked(&buf[..offset1]) };
        info!(&ctx, "name, len: {}, str: {}", offset1, name);
        let name = unsafe { core::str::from_utf8_unchecked(&buf[offset1..offset1+offset2]) };
        info!(&ctx, "argv0, len: {}, str: {}", offset2, name);
    }
    0
}


#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    unsafe { core::hint::unreachable_unchecked() }
}
