#![no_std]
#![no_main]

use aya_bpf::{
    helpers::*,
    maps::PerCpuArray,
    macros::{map, tracepoint},
    programs::TracePointContext,
};

#[map]
static mut BUFFER: PerCpuArray<[u8; 4096]> = PerCpuArray::with_max_entries(1, 0);

#[tracepoint(name="tracepoint_execve")]
pub fn execve(ctx: TracePointContext) -> u32 {
    let offset_arg_name = 16;

    if let Some(buffer_ptr) = unsafe { BUFFER.get_ptr_mut(0) }{
        let buffer = unsafe { &mut *buffer_ptr };

        /* read filename the first time */
        let (mut _buf, mut buf1) = buffer.split_at_mut(0);
        let addr = unsafe { match ctx.read_at::<u64>(offset_arg_name){
                Ok(v) => v,
                _ => return 1
            }
        };
        let len_name = unsafe { match bpf_probe_read_str(addr as *const u8, &mut buf1){
                Ok(v) => v,
                _ => return 1
            }
        };


        /* read filename the second time */
        //let len_name = 100;
        if len_name > 4096 {
            return 1
        }

        let (mut _buf, mut buf2) = buf1.split_at_mut(len_name);
        let len_name = unsafe { match bpf_probe_read_str(addr as *const u8, &mut buf2){
                Ok(v) => v,
                _ => return 1
            }
        };
    }
    0
}


#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    unsafe { core::hint::unreachable_unchecked() }
}
