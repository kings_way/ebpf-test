#include <linux/bpf.h>
#include <bpf/bpf_helpers.h>

char LICENSE[] SEC("license") = "Dual BSD/GPL";

// from vmlinux.h, bpftool btf dump file /sys/kernel/btf/vmlinux format c > vmlinux.h
struct trace_event_raw_sys_enter {
	short unsigned int type;
	unsigned char flags;
	unsigned char preempt_count;
	int pid;
	int __syscall_nr;
	long unsigned int args[6];
	char __data[0];
};
struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
	__uint(max_entries, 1);
	__type(key, unsigned int);
	__type(value, char [4096]);
} map SEC(".maps");

SEC("tracepoint/syscalls/sys_enter_execve")
int tracepoint__syscalls__sys_enter_execve(struct trace_event_raw_sys_enter *ctx)
{
	bpf_printk("----------------- execve ---------------");
	int pid = bpf_get_current_pid_tgid() >> 32;

	int key = 0 ;
	char *buf = (char *)bpf_map_lookup_elem(&map, &key);	// get ptr of inner buffer
	if (buf == 0)
		return 0;

	char *ptr_name = (char *)ctx->args[0];
	char **argv = (char **)ctx->args[1];
	char *ptr_argv0, *ptr_argv1;
	bpf_probe_read(&ptr_argv0, sizeof(ptr_argv0), argv + 0);
	bpf_probe_read(&ptr_argv1, sizeof(ptr_argv1), argv + 1);
	bpf_printk("pid:%d, direct_read: %s, %s, %s", pid, ptr_name, ptr_argv0, ptr_argv1);

	/* read filename into buffer */
//	int offset = 
//	unsigned int offset = 
	unsigned long long offset =
		bpf_probe_read_str(buf, 2048, ptr_name);
	bpf_printk("pid:%d, name: %d, %s", pid, offset, buf);
	
	/* read argv0 into buffer */
	if (offset > 2048)
		return 0;
	unsigned long long offset2 = bpf_probe_read_str(buf + offset, 2048, ptr_argv0);
	bpf_printk("pid:%d, argv0: %d, %s", pid, offset2, buf + offset );

	// read argv1 into buffer
	if (offset2 > 2048)
		return 0;
	unsigned long long offset3 = bpf_probe_read_str(buf + offset2, 2048, ptr_argv1);
	bpf_printk("pid:%d, argv1: %d, %s", pid, offset3, buf + offset2 );

	return 0;
}
